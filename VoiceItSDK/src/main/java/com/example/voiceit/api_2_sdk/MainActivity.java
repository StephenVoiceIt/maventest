package com.example.voiceit.api_2_sdk;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    public VoiceItAPI2 myVoiceIt2;
    public String userId = "usr_047a6db16bc84150a8f367289df39061";
    public Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myVoiceIt2 = new VoiceItAPI2("key_f3a9fb29944a4e4180d4c98e7f03c713",
                "tok_2b13ba7ac80741c98113c301da31001b");
        mActivity = this;
    }

    public void VideoEnrollmentView(View view) {
        myVoiceIt2.enrollUserView(mActivity, userId, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                System.out.println("VideoEnrollmentView JSONResult : " + response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    System.out.println("VideoEnrollmentView JSONResult : " + errorResponse.toString());
                }
            }
        });
    }

    public void VideoVerificationView(View view) {
        myVoiceIt2.verifyUserView(this, userId, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                System.out.println("VideoVerificationView JSONResult : " + response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    System.out.println("VideoVerificationView JSONResult : " + errorResponse.toString());
                }
            }
        });
    }
}


